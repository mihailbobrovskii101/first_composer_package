<?php

namespace Simplexi\Greetr;

class Greetr
{
    public function greet(String $sName)
    {
        return 'Привет' . $sName . ', это мой первый composer package';
    }
}